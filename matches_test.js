let index = require("./index.js"),
  assert = require("assert")

describe("idOfTeamP", function() {
  let idOfTeamP=index.idOfTeamP
  it("returns 43971 for Switzerland", function() {
    return idOfTeamP("Switzerland", "en")
      .then((id) => assert.equal(id, 43971))
  })
})
