//  api/v1/teams/seasons/{idTeam}?language={language}&count={count}
// idTeam SWITZERLAND; 43971
const request = require( 'request' ),
  rp = require("request-promise-native")
module.exports.idOfTeamP = idOfTeamP

function idOfTeamP(name, inLanguage) {
  return rp("https://api.fifa.com/api/v1/teams/search?name=" + name + "&count=1&language=" + inLanguage)
  .then(function(response) {
    return JSON.parse(response).Results[0].IdTeam
  })
}

function getSeasons(id) {
  return rp("https://api.fifa.com/api/v1/teams/seasons/" + id)
  .then(function(response) {
    return JSON.parse(response).Results.map(r=>r.IdSeason)
  })
}

function getGames(idSeason) {
   // https://api.fifa.com/api/v1/calendar/matches?idseason=254645&idcompetition=17
   return rp(`https://api.fifa.com/api/v1/calendar/matches?idseason=${idSeason}`)
   .then(function(response) {
     console.log("++++++++++++++++++++++++" + JSON.parse(response).Results[0].IdTeam)
     return JSON.parse(response).Results[0].IdTeam
   })
}

function getGamesbyTeam(IdTeam) {
   // https://api.fifa.com/api/v1/calendar/matches?idseason=254645&idcompetition=17
   return rp(`https://api.fifa.com/api/v1/calendar/matches?idTeam=${IdTeam}`)
   .then(function(response) {
     return JSON.parse(response).Results
   })
}
// api/v1/teams/seasons/{idTeam}?language={language}&count={count} Retrieves all season played by team.
function getSeasonsByTeam (IdTeam) {
  return rp (`https://api.fifa.com/api/v1/teams/seasons/${IdTeam}`)
  .then(function(response) {
    return JSON.parse(response).Results
  })
}

idOfTeamP("Switzerland", "en").then(getSeasonsByTeam).then(console.log)
//idOfTeamP("Switzerland", "en").then(getGamesbyTeam).then(console.log)
idOfTeamP("Switzerland", "en").then(console.log)
